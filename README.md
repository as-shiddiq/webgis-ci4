# WEBGIS CODEIGNITER CI4
Aplikasi GIS berbasis web standar yang dapat dikembangkan menjadi aplikasi yang lebih bermanfaat kedepannya.

# Spesifikasi dan Informasi
1. Menggunakan Bahasa Pemrograman PHP >7.4
2. Codeigniter 4.1.9
2. Package yang digunakan diinstall menggunakan [Composer](https://getcomposer.org/)
3. Database MySQL (include dari [Laragon](https://laragon.org/)) - 

# Komponen UI/CSS
1. Template by [Mazer](https://github.com/zuramai/mazer) base menggunakan [Bootstrap 5](https://getbootstrap.com/) .
2. Icon by [Bootstrap Icon](https://icons.getbootstrap.com/)

# Komponen Javascript
1. Popup Boxes, Alert by [SweetAlert](https://sweetalert2.github.io/) - VanillaJS
2. Form Validation by [PristineJs](https://pristine.js.org/) - VanillaJs
# Tutorial WebGIS CodeIgniter CI4
1. Konfigurasi Template
    - [#1 Instalasi & Konfigurasi Template](https://youtu.be/r4F7rrSSCr0)
2. Kategori
    - [#2.1 Create & Read (CRUD Kategori)](https://youtu.be/WOYmHa-1wIk)
    - [#2.2 Update & Delete (CRUD Kategori)](https://youtu.be/8IUIjkeDzvo)
    - [#2.3 Form Validation & Alert (CRUD Kategori)](https://youtu.be/WTEBGqSzayA)
3. Sub Kategori 
    - [#3.1 CRUD (Sub Kategori)](https://youtu.be/SvscF2zIBPY)
    - [#3.2 Upload Ikon (Sub Kategori)](https://youtu.be/fy_O4yXuPvQ)
3. Pengguna
    - [#4.1 (CRUD Pengguna)](https://youtu.be/27-5D-0Y9nk)
    - [#4.2 Validasi Username (CRUD Pengguna)](https://youtu.be/anHXxYBbkoM)
    - [#4.3 Validasi Username PristineJs (CRUD Pengguna)](https://youtu.be/lnksju82pyM)
3. []()

# Informasi lainnya
untuk melihat project public yang telah aku buat, silakan cek langsung di akun GitlabKu di https://gitlab.com/as-shiddiq

# Donate
Kalian bisa langsung donate ke https://trakteer.id/as.shiddiq
atau langsung beri tips melalui https://trakteer.id/as.shiddiq/tip

Copyright © 2022 [Nasrullah Siddik](bit.ly/YTNSiddik). All rights reserved.


